from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.urls import reverse
# Create your views here.

def user_login(request):
    context = {}
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username = username, password = password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('user_success'))
        else:
            context["error"] = "Kalo mau pake akun ini bikin di py manage.py createsuperuser dulu bro"
            return render(request, "Login.html", context)
    else:
        return render(request, "Login.html", context)

def success(request):
    context = {}
    context['user'] = request.user
    return render(request, "Logout.html", context)

    
def user_logout(request):
    if request.method == "POST":
        logout(request)
        return HttpResponseRedirect(reverse("user_login"))